#!/bin/bash

if [ -n "$CODER" ]; then
  # mv /home/coder/.gitconfig /home/coder/.gitconfig.bak
  mkdir -p /home/coder/.ssh
  ln -s /home/coder/.config/coderv2/dotfiles/.ssh/config /home/coder/.ssh/config
  # cp /home/coder/.config/coderv2/dotfiles/hooks.sh /workspace/remote-dev/hooks.sh

  # Use Administrator super-powers
  #awk '{gsub(/dev-temp-privileged/,"default")};1' /home/coder/.aws/config > /home/coder/.aws/config.tmp && mv /home/coder/.aws/config.tmp /home/coder/.aws/config
  #awk '{gsub(/DeveloperAccess/,"AdministratorAccess")};1' /home/coder/.aws/config > /home/coder/.aws/config.tmp && mv /home/coder/.aws/config.tmp /home/coder/.aws/config
else
  echo "Not running in Coder or Gitpod"
fi